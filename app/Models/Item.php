<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'price',
        'desc',
    ];

    public function product()
    {
        return $this->belongsToMany(Product::class, 'products_item', 'item_id', 'product_id');
    }
}
