<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'email',
        'user_type',
        'birthday',
        'birth_place',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
