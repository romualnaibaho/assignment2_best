<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'desc',
    ];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'products_item', 'product_id', 'item_id');
    }
}
