<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Item;

class ProductController extends Controller
{
    public function maintain_product($id)
    {
        // With Query Builder
        $items_id = DB::table('products_item')
                        ->where('product_id', $id)
                        ->select('item_id')
                        ->get();
        $ids = [];
        foreach($items_id as $id_item){
            $ids[] = $id_item->item_id;
        }
        
        $items_queryB = DB::table('items')
                        ->whereIn('id', $ids)
                        ->get();
                        
        $items_ORM = Product::find($id)->items;
        $product = Product::find($id);

        return view('pages.admin.items', compact('items_queryB', 'items_ORM', 'product'));
    }

    public function create_product(Request $request)
    {
        Product::insert([
            'name' => $request->name,
            'desc' => $request->desc
        ]);

        return redirect()->route('products');
    }

    public function create_item(Request $request)
    {
        // With Query Builder
        // $item = DB::table('items')->insert([
        //     'name' => $request->name,
        //     'price' => $request->price,
        //     'desc' => $request->desc
        // ]);
        // $item_id = DB::getPdo()->lastInsertId();

        // DB::table('products_item')->insert([
        //     'product_id' => $request->product_id,
        //     'item_id' => $item_id,
        // ]);

        // With ORM
        $item = Item::create([
            'name' => $request->name,
            'price' => $request->price,
            'desc' => $request->desc
        ]);
        $product = Product::find($request->product_id);
        $product->items()->attach($item);

        return redirect()->route('maintain-product', ['id' => $request->product_id]);
    }
}