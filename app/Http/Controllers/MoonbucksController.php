<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\Product;
use Hash;

class MoonbucksController extends Controller
{
    public function view_home()
    {
        return view('pages.home');
    }

    public function view_team()
    {
        return view('pages.team_member');
    }

    public function view_promos_news()
    {
        return view('pages.promos_news');
    }

    public function view_products()
    {
        $products_queryB = DB::table('products')->get();
        $products_ORM = Product::get();

        return view('pages.admin.products', compact('products_queryB', 'products_ORM'));
    }

    public function join_us()
    {
        return view('pages.join_us');
    }

    public function view_register()
    {
        return view('pages.user.register');
    }

    public function create_account(Request $request)
    {

        $request->validate([
            'name' => 'required|max:25|min:5',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            'token' => bcrypt($request->email)
        ]);

        $creation = User::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'token' => bcrypt($request->email)
        ]);

        if($creation)
        {
            $user = User::where('email', $request->email)->first();

            $details = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email
            ];
           
            \Mail::to($user->email)->send(new \App\Mail\VerifyEmail($details));
        } 
        else
        {
            return redirect()->back();
        }

        return view('pages.info.mail_info', compact('user'));
    }

    public function verify_email($email)
    {
        $user = User::where('email', $email)->first();

        if($user)
        {
            $user->update([
                'email_verified' => 1,
            ]);

            Profile::insert([
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email
            ]);

            return redirect()->route('login');
        }

        return "ERROR FOUND";
    }

    public function home()
    {
        return view('pages.user.dashboard');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);

        $attemps = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $user = User::where('email', $request->email)->first();

        if($user)
        {
            if($user->email_verified == 1)
            {
                if(\Auth::attempt($attemps, (bool) $request->remember))
                {
                    return redirect()->route('home');
                }
                else{
                    return redirect()->back()->withErrors(['errors' => 'Wrong Password.']);
                }
            }
            else
            {
                return redirect()->back()->withErrors(['errors' => 'Please verify your email first.']);
            }
        }

        return "ERROR FOUND";
    }

    public function view_profile($id)
    {
        $user = User::where('id', $id)->first();
        $profile = Profile::where('user_id', $user->id)->first();

        return view('pages.user.profile', compact('profile', 'user'));
    }

    public function edit_profile($id)
    {
        $profile = Profile::where('id', $id)->first();

        return view('pages.user.edit_profile', compact('profile'));
    }

    public function update_profile(Request $request)
    {
        $profile = Profile::where('id', $request->id)->first();
        $user = User::where('id', $profile->user_id);

        $request->validate([
            'name' => 'required|max:25|min:5'
        ]);

        $user->update([
            'name' => $request->name
        ]);

        $profile->update([
            'name' => $request->name,
            'user_type' => $request->user_type,
            'birthday' => $request->birthday,
            'birth_place' => $request->birth_place
        ]);

        return redirect()->route('view-profile', $profile->user_id);
    }

    public function delete_profile($id)
    {
        $profile = Profile::where('id', $id)->first();
        $user = User::where('id', $profile->user_id);

        $profile->delete();
        $user->delete();

        return redirect()->route('login');
    }

    public function logout(){
        \Auth::logout();

        return redirect()->route('login');
    }
}
