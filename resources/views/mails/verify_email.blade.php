<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moonbucks</title>
</head>
<body>
    <h1>Hai {{ $details['name'] }}</h1>
    <hr>
    <p>Please click the link below to verify your email!</p>
    <a href="{{ route('verify-me', $details['email']) }}"><b>Verify Me !</b></a>
</body>
</html>