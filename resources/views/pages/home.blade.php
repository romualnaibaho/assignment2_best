
@extends('layouts.moonbucks')

@section('content')

<!-- Header -->

<header id="home">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="header-text col-12">
                        <h2>the best quality</h2>
                        <p>get the perfect cup of coffee to live your day</p>
                        <br>
                        <br>
                        <p style="font-weight: bold;font-style: italic;">
                            Moonbucks Cafe, 123 Main Street, New York, NY 10030
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End of Header -->

    <!-- Main Content -->

    <div class="my-content">
        <div id="about" class="about container">
            <h2>About Us</h2>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            <br> <br>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            <br> <br>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
        </div>

        <div id="pricing" class="pricing">
            <h2>Pricing</h2>
            <div class="container">
                <div class="row">
                    <div class="item col-12 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="Item 1">
                        <h3>$ 50.00</h3>
                        <h5>Smile Coffee</h5>
                        <p>aromatic, sweet, and extra chocolate</p>
                    </div>
                    <div class="item col-12 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="Item 2">
                        <h3>$ 50.00</h3>
                        <h5>Smile Coffee</h5>
                        <p>aromatic, sweet, and extra chocolate</p>
                    </div>
                    <div class="item col-12 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="Item 3">
                        <h3>$ 50.00</h3>
                        <h5>Smile Coffee</h5>
                        <p>aromatic, sweet, and extra chocolate</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact" class="contact">
            <div class="container">
                <h2>Contact Us</h2>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <input id="frontname" type="text" placeholder="Nama Depan" required>
                    </div>
                    <div class="col-12 col-md-6">
                        <input id="lastname" type="text" placeholder="Nama Belakang" required>
                    </div>
                    <div class="col-12">
                        <textarea name="body-email" id="body-email" cols="30" rows="10" placeholder="Send us anything..." required></textarea>
                    </div>
                    <div class="col-12">
                        <input onclick="send_email()" type="submit" value="Send Email">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Of Main Content -->

    <script>
        window.onscroll = function() {
            var navbar = document.getElementById("my-navbar");
            var sticky = navbar.offsetTop; 

            if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky")
                document.getElementById("my-logo").src = "{{ asset('img/moonbucks2.png') }}";
            } else {
                navbar.classList.remove("sticky");
                document.getElementById("my-logo").src = "{{ asset('img/moonbucks1.png') }}";
            }
        };
    </script>
@endsection