@extends('layouts.moonbucks')

@section('content')

<!-- Team Member -->

<div class="team-member">
    <h1>Team Member</h1>
    <h6>Introduce our incredible team 
        <br>
        from over the world!
    </h6>

    <div class="teams">
        <div class="container">
            <div class="row justify-content-center">
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 1">
                    <h5>CEO</h5>
                    <h7>Robert Pattinson</h7>
                    <p>be kind, always</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 2">
                    <h5>Herry Striker</h5>
                    <h7>Marketing Manager</h7>
                    <p>we are the world's biggest things</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Fanny Livingstone</h5>
                    <h7>Sales Marketing</h7>
                    <p>be humble and stay calm.</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Simon Trunitty</h5>
                    <h7>Coffee Maker Manager</h7>
                    <p>life is simple, dont make it be complicated.</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 1">
                    <h5>CEO</h5>
                    <h7>Robert Pattinson</h7>
                    <p>be kind, always</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 2">
                    <h5>Herry Striker</h5>
                    <h7>Marketing Manager</h7>
                    <p>we are the world's biggest things</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Fanny Livingstone</h5>
                    <h7>Sales Marketing</h7>
                    <p>be humble and stay calm.</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Simon Trunitty</h5>
                    <h7>Coffee Maker Manager</h7>
                    <p>life is simple, dont make it be complicated.</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Fanny Livingstone</h5>
                    <h7>Sales Marketing</h7>
                    <p>be humble and stay calm.</p>
                </div>
                <div class="item col-6 col-md-3">
                    <img src="{{ asset('img/team.png') }}" alt="Item 3">
                    <h5>Simon Trunitty</h5>
                    <h7>Coffee Maker Manager</h7>
                    <p>life is simple, dont make it be complicated.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End of Team Member -->

<script>
    document.getElementById("my-navbar").classList.add("sticky");
</script>

@endsection