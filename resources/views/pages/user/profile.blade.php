@extends('layouts.user_dashboard')

@section('content')

<div class="profile" style="padding:120px 0px">
    <div class="container">
        <div class="profile-card">
            <h2>Profile</h2>
            <hr>

            <div class="row justify-content-center">
                <div class="col-12 col-md-6" style="border:1px solid grey; border-radius: 10px; padding:20px 20px">
                    <table style="width:100%;">
                        <tr>
                            <th style="width:30%">Full Name</th>
                            <th style="width:10%">:</th>
                            <th>{{ $profile->name }}</th>
                        </tr>
                        <tr>
                            <th style="width:30%">Email</th>
                            <th style="width:10%">:</th>
                            <th>{{ $profile->email }}</th>
                        </tr>
                        <tr>
                            <th style="width:30%">User Type</th>
                            <th style="width:10%">:</th>
                            <th>{{ $profile->user_type }}</th>
                        </tr>
                        <tr>
                            <th style="width:30%">Birth Day</th>
                            <th style="width:10%">:</th>
                            <th>{{ $profile->birthday }}</th>
                        </tr>
                        <tr>
                            <th style="width:30%">Birth Place</th>
                            <th style="width:10%">:</th>
                            <th>{{ $profile->birth_place }}</th>
                        </tr>
                    </table>
                    <div class="text-center mt-4">
                        <a href="{{ route('edit-profile', $profile->id) }}"><button type="button" class="btn btn-info btn-sm">Edit Profile</button></a>
                        |
                        <a href="{{ route('delete-profile', $profile->id) }}" onclick="return confirm('Are you sure you want to delete your account?');"><button type="button" class="btn btn-danger btn-sm">Delete Account</button></a>
                    </div>
                </div>
            </div>

            <hr>
        </div>
    </div>
</div>

@endsection