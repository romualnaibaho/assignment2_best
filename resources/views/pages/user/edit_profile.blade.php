@extends('layouts.autentikasi')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Join Us</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style_2.css')}}">
</head>
<body>
    <div class="container">
        <div class="card-edit mt-4">
            <div class="text-center mt-4 mb-4">
                <img src="{{ asset('img/moonbucks1.png') }}" alt="Logo">
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-md-7" style="border:1px solid grey; border-radius: 10px; padding:20px 20px; background-color: #fff">
                    <form method="POST" action="{{ route('update-profile') }}">
                        @csrf
                        <table style="width:100%;">
                            <input type="hidden" class="form-control" id="id" name="id" value="{{ $profile->id }}">
                            <tr>
                                <th style="width:30%">Full Name</th>
                                <th style="width:10%">:</th>
                                <th>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="{{ $profile->name }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong style="color:red;font-size:14px">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </th>
                            </tr>
                            <tr>
                                <th style="width:30%">Email</th>
                                <th style="width:10%">:</th>
                                <th><input readonly type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $profile->email }}"></th>
                            </tr>
                            <tr>
                                <th style="width:30%">User Type</th>
                                <th style="width:10%">:</th>
                                <th><input type="User Type" class="form-control" id="user_type" name="user_type" placeholder="User Type" value="{{ $profile->user_type }}"></th>
                            </tr>
                            <tr>
                                <th style="width:30%">Birth Day</th>
                                <th style="width:10%">:</th>
                                <th><input type="text" class="form-control" id="birthday" name="birthday" placeholder="[01 January 2000]" value="{{ $profile->birthday }}"></th>
                            </tr>
                            <tr>
                                <th style="width:30%">Birth Place</th>
                                <th style="width:10%">:</th>
                                <th><input type="text" class="form-control" id="birth_place" name="birth_place" placeholder="Birth Place" value="{{ $profile->birth_place }}"></th>
                            </tr>
                        </table>
                        <div class="form-group mt-4">
                            <input type="submit" class="btn btn-secondary btn-sm" value="Edit" style="width:100%">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
