@extends('layouts.user_dashboard')

@section('content')

<div class="products" style="padding: 100px 0px">
    <div class="container text-center text-black">
        <h1>All Products</h1>

        <div class="row justify-content-center mt-4">
            <div class="col-6 col-md-4">
                <div class="card" style="width: 100%;">
                    <img src="https://i.pinimg.com/originals/74/8c/26/748c264044d4f0ce13de9773d067a5b6.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Drinks</h5>
                        <p class="card-text">50 items</p>
                        <a href="#" class="btn btn-primary">View all items</a>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
            <div class="card" style="width: 100%;">
                <img src="https://ftp.qaayima.com/uploads/FarujAlBaik/Chicken_Handi.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Foods</h5>
                    <p class="card-text">50 items</p>
                    <a href="#" class="btn btn-primary">View all items</a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection