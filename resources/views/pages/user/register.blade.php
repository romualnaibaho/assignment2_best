@extends('layouts.autentikasi')

@section('content')

<div class="join-card col-10 col-md-4">
    <div class="text-center">
        <img src="{{ asset('img/moonbucks2.png') }}" alt="Logo">
    </div>
    <hr>
    <div class="text-center">
        <p>Register</p>
    </div>
    <form method="POST" action="{{ route('create-account') }}">
        @csrf
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="{{ old('name') }}">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong style="color:red;font-size:14px">{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong style="color:red;font-size:14px">{{ $errors->first('mail') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong style="color:red;font-size:14px">{{ $errors->first('password') }}</strong>
                    <p style="color:grey;font-size:12px">The password must contain letter and number.</p>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Retype Password">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-secondary btn-sm" value="Register" style="width:100%">
        </div>
    </form>
    <hr>
    <div class="row text-center">
        <div class="col-12">
            <a href="#">Already have an account?</a>
        </div>
    </div>
</div>

@endsection