@extends('layouts.autentikasi')

@section('content')

<div class="join-card col-10 col-md-4">
    <div class="text-center">
        <img src="{{ asset('img/moonbucks2.png') }}" alt="Logo">
    </div>
    <hr>
    <div class="text-center">
        <p>Login</p>
    </div>
    @if(count($errors) > 0 )
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <ul class="p-0 m-0" style="list-style: none;">
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
    </div>
    @endif
    <form method="POST" action="{{ route('post-login') }}">
        @csrf
        <div class="form-group">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-secondary btn-sm" value="Login" style="width:100%">
        </div>
    </form>
    <hr>
    <div class="row text-center">
        <div class="col-6">
            <a href="{{ route('view-register') }}">Register here</a>
        </div>
        <div class="col-6">
            <a href="#">Forget Password</a>
        </div>
    </div>
</div>

@endsection