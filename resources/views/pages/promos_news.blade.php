@extends('layouts.moonbucks')

@section('content')

<!-- Promo and News -->

<div class="promos">
    <!-- BsT Carousel -->
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="{{ asset('img/promo1.png') }}" class="d-block w-100" alt="Promo 1">
            <div class="carousel-caption d-none d-md-block">
                <h1>Promo 1</h1>
                <h4>20% Off</h4>
                <h5>every Saturday Night for milky coffee</h5>
            </div>
            </div>
            <div class="carousel-item">
            <img src="{{ asset('img/promo2.png') }}" class="d-block w-100" alt="Promo 2">
            <div class="carousel-caption d-none d-md-block">
                <h1>Promo 2</h1>
                <h4>Independence Off</h4>
                <h5>up to 40% off if you buy more than 10 cups</h5>
            </div>
            </div>
            <div class="carousel-item">
            <img src="{{ asset('img/promo3.png') }}" class="d-block w-100" alt="Promo 3">
            <div class="carousel-caption d-none d-md-block">
                <h1>Promo 3</h1>
                <h4>15% Off</h4>
                <h5>pay with Moonbucks Card</h5>
            </div>
            </div>
        </div>
    </div>
    <!-- End of Carousel -->

    <!-- News -->
    <div class="container mt-4">
        <h2>Breaking News</h2>
        <hr>
        <div class="row news">
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 1</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 2</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 3</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 4</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 5</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 6</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 7</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 8</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="row">
                    <div class="col-4 col-md-4">
                        <img src="{{ asset('img/item.png') }}" alt="News 1">
                    </div>
                    <div class="col-8 col-md-8">
                        <h4><b>This is news 9</b></h4>
                        <p>This is the preview of news one, maybe some word or something...</p>
                        <a href="#">read more...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End of Promo and News -->

<script>
    document.getElementById("my-navbar").classList.add("sticky");
</script>

@endsection