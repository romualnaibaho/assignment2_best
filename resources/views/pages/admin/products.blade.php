@extends('layouts.user_dashboard')

@section('content')

<div class="products" style="padding: 100px 0px">
    <div class="container text-center text-black">
        <h1>Maintain Products</h1>

        <h2 class="mt-4 mb-4">With Query Builder</h2>
        <hr>
        <div class="row justify-content-center mt-4">
            @forelse ($products_queryB as $product)
                <div class="col-6 col-md-4">
                    <div class="card" style="width: 100%;">
                        <img src="https://i.pinimg.com/originals/74/8c/26/748c264044d4f0ce13de9773d067a5b6.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p>{{ $product->desc }}</p>
                            <p class="card-text">50 items</p>
                            <a href="{{ route('maintain-product', $product->id) }}" class="btn btn-primary">Maintain</a>
                        </div>
                    </div>
                </div>
            @empty
                <p>Empty Products !</p>
            @endforelse
        </div>

        <h2 class="mt-4 mb-4">With ORM</h2>
        <hr>
        <div class="row justify-content-center mt-4">
            @forelse ($products_ORM as $product)
                <div class="col-6 col-md-4">
                    <div class="card" style="width: 100%;">
                        <img src="https://i.pinimg.com/originals/74/8c/26/748c264044d4f0ce13de9773d067a5b6.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p>{{ $product->desc }}</p>
                            <p class="card-text">50 items</p>
                            <a href="{{ route('maintain-product', $product->id) }}" class="btn btn-primary">Maintain</a>
                        </div>
                    </div>
                </div>
            @empty
                <p>Empty Products !</p>
            @endforelse
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-12 col-md-8">
                <div class="make-product text-left" style="border: 1px solid grey; padding:20px; border-radius:10px">
                    <form methods="POST" action="{{ route('create-product') }}">
                        @csrf
                        <h4 class="mb-4" style="text-align:center">Add Products</h4>
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Drinks">
                        </div>
                        <div class="form-group">
                            <label for="desc">Description</label>
                            <textarea class="form-control" name="desc" rows="3" placeholder="Including hot and cold drinks"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Add Product</button>     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection