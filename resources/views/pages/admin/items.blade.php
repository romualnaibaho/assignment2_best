@extends('layouts.user_dashboard')

@section('content')

<div class="products" style="padding: 100px 0px">
    <div class="container text-center text-black">
        <h1>Maintain Products</h1>

        <h2 class="mt-4 mb-4">With Query Builder</h2>
        <hr>
        <div class="row justify-content-center mt-4">
            @forelse ($items_queryB as $item)
                <div class="col-6 col-md-4">
                    <div class="card" style="width: 100%;">
                        <img src="https://i.pinimg.com/originals/74/8c/26/748c264044d4f0ce13de9773d067a5b6.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->name }}</h5>
                            <p>{{ $item->desc }}</p>
                            <p class="card-text">50 items</p>
                        </div>
                    </div>
                </div>
            @empty
                <p>Empty Item !</p>
            @endforelse
        </div>

        <h2 class="mt-4 mb-4">With ORM</h2>
        <hr>
        <div class="row justify-content-center mt-4">
            @forelse ($items_ORM as $item)
                <div class="col-6 col-md-4">
                    <div class="card" style="width: 100%;">
                        <img src="https://i.pinimg.com/originals/74/8c/26/748c264044d4f0ce13de9773d067a5b6.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->name }}</h5>
                            <p>{{ $item->desc }}</p>
                            <p class="card-text">50 items</p>
                        </div>
                    </div>
                </div>
            @empty
                <p>Empty Item !</p>
            @endforelse
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-12 col-md-8">
                <div class="make-product text-left" style="border: 1px solid grey; padding:20px; border-radius:10px">
                    <form methods="POST" action="{{ route('create-item') }}">
                        @csrf
                        <h4 class="mb-4" style="text-align:center">Add Items to {{ $product->name }}</h4>
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label for="name">Item Name</label>
                                <input type="text" class="form-control mb-2" name="name" placeholder="Coffee Mocca">
                            </div>
                            <div class="col-auto">
                                <label for="price">Price</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                    </div>
                                    <input type="text" class="form-control" name="price" placeholder="10.00">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc">Description</label>
                            <textarea class="form-control" name="desc" rows="3" placeholder="Mocca Coffee with extra caramel and chocolate."></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Add Item</button>     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection