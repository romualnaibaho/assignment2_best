<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Landing Page</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

    <!-- Navbar -->

<nav id="my-navbar" class="my-navbar sticky">
        <div class="logo">
            <a href="#">
                <img id="my-logo" src="{{ asset('img/moonbucks2.png') }}" alt="moonbucs" style="height: auto;">
            </a>
        </div>
        
        <ul class="nav-links">
            <!-- USING CHECKBOX HACK -->
            <input type="checkbox" id="checkbox_toggle" />
            <label for="checkbox_toggle" class="hamburger">&#9776;</label>
            <!-- NAVIGATION MENUS -->
            <div class="menu">
                <li><a href="{{ route('home') }}">Dashboard</a></li>
                <li><a href="{{ route('view-profile', \Auth::User()->id) }}">Profile</a></li>
                @if(\Auth::User()->role == 'admin')               
                <li><a href="{{ route('products') }}">Products</a></li>
                @endif
                <li class="register"><a href="{{ route('logout') }}">Logout</a></li>
            </div>
        </ul>
    </nav>

    <!-- End of Navbar -->

    @yield('content')

    <!-- Footer -->

<footer>
        <div class="container">
            <img src="{{ asset('img/moonbucks1.png')}}" alt="Moonbucks">
            <div class="row" style="margin-top: 40px;">
                <div class="info col-6 col-md-3">
                    Moonbucks Cafe
                    <br>
                    <br>
                    123 Main Street, New York, NY 10030
                    <br>
                    Phone: 121202
                    <br>
                    Mail: admin@moonbucks.com
                </div>
                <div class="info2 col-6 col-md-3">
                    <h5><b><u>Info</u></b></h5>
                    <ul>
                        <li>
                            <a href="#">Pricing</a>
                        </li>
                        <li>
                            <a href="#">Order</a>
                        </li>
                        <li>
                            <a href="#">Partner</a>
                        </li>
                    </ul>
                </div>
                <div class="info2 col-6 col-md-3">
                    <h5><b><u>Help</u></b></h5>
                    <ul>
                        <li>
                            <a href="#">FAQ</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li>
                            <a href="#">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="info2 col-6 col-md-3">
                    <h5><b><u>Branch</u></b></h5>
                    <ul>
                        <li>
                            <a href="#">Los Angeles</a>
                        </li>
                        <li>
                            <a href="#">Texas</a>
                        </li>
                        <li>
                            <a href="#">Pensylvania</a>
                        </li>
                        <li>
                            <a href="#">Ohio</a>
                        </li>
                        <li>
                            <a href="#">Indonesia</a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <div style="text-align: center; font-size: 12px; color: #fff;margin-top: 40px;">
                &copy; 2022 Moonbucks Cafe. All Rights Reserved.
            </div>
        </div>
    </footer>

    <!-- End of Footer -->
</body>
</html>