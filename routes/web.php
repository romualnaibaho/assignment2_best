<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MoonbucksController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MoonbucksController::class, 'view_home'])->name('home')->middleware('guest');

Route::get('/team-member', [MoonbucksController::class, 'view_team'])->name('team-member')->middleware('guest');

Route::get('/promo-and-news', [MoonbucksController::class, 'view_promos_news'])->name('promo-news')->middleware('guest');

Route::get('/login', [MoonbucksController::class, 'join_us'])->name('login')->middleware('guest');

Route::POST('/post-login', [MoonbucksController::class, 'login'])->name('post-login');

Route::get('/home', [MoonbucksController::class, 'home'])->name('home')->middleware('auth');

Route::get('/register', [MoonbucksController::class, 'view_register'])->name('view-register')->middleware('guest');

Route::POST('/create-account', [MoonbucksController::class, 'create_account'])->name('create-account');

Route::get('/verify-email/{email}', [MoonbucksController::class, 'verify_email'])->name('verify-me');

Route::get('/profile/{id}', [MoonbucksController::class, 'view_profile'])->name('view-profile')->middleware('auth');

Route::get('/edit/profile/{id}', [MoonbucksController::class, 'edit_profile'])->name('edit-profile')->middleware('auth');

Route::POST('/update/profile', [MoonbucksController::class, 'update_profile'])->name('update-profile');

Route::get('/delete/profile/{id}', [MoonbucksController::class, 'delete_profile'])->name('delete-profile')->middleware('auth');

Route::get('/logout', [MoonbucksController::class, 'logout'])->name('logout')->middleware('auth');

Route::get('/products', [MoonbucksController::class, 'view_products'])->name('products')->middleware('auth');

Route::get('/product-list/{id}', [ProductController::class, 'maintain_product'])->name('maintain-product')->middleware('auth');

Route::get('/create/product', [ProductController::class, 'create_product'])->name('create-product')->middleware('auth');

Route::get('/create/item/product/', [ProductController::class, 'create_item'])->name('create-item')->middleware('auth');